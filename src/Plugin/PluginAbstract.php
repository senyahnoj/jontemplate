<?php
namespace JonTemplate\Plugin;

/**
 * Abstract class from which all plugins inherit
 */
abstract class PluginAbstract
{
    /**
     * @var PluginAbstract instance of self
     */
    protected static $_instances = array();

    /**
     * Returns singleton instance of self
     *
     * @return PluginAbstract
     */
    final public static function getInstance()
    {
        $class = get_called_class();
        if (!isset(static::$_instances[$class])) {
            static::$_instances[$class] = new $class();
        }
        return static::$_instances[$class];
    }
}
