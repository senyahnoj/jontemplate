<?php
namespace JonTemplate\Plugin;

/**
 * Plugin to add a cache buster to links etc.
 */
class CacheBuster extends PluginAbstract
{
    /**
     * @var string character set
     */
    protected $_cacheBuster = 'd9699ad';

    /**
     * To string method - as the function may be called without arguments this is
     * used instead of a direct() method
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getCacheBuster();
    }

    /**
     * Sets the cache buster string
     *
     * @param string $cacheBuster the cache buster string
     * @return CacheBuster
     * @throws \UnexpectedValueException
     */
    public function setCacheBuster($cacheBuster)
    {
        if (ctype_alnum($cacheBuster) === false) {
            throw new \UnexpectedValueException('Non-alphanumeric characters not allowed');
        }
        $this->_cacheBuster = $cacheBuster;
        return $this;
    }

    /**
     * Returns the cache buster string
     *
     * @return string
     */
    public function getCacheBuster()
    {
        return $this->_cacheBuster;
    }
}
