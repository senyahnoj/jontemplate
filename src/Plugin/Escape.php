<?php
namespace JonTemplate\Plugin;

/**
 * escape plugin
 */
class Escape extends PluginAbstract
{
    /**
     * @var string character set
     */
    protected $_charset = 'UTF-8';

    /**
     * Direct method - call the HTML escaping
     *
     * @param string $string string to escape
     * @return string
     */
    public function direct($string)
    {
        return htmlspecialchars($string, ENT_QUOTES, $this->_charset);
    }

    /**
     * Escapes html using htmlspecialchars
     *
     * @param string $string string to escape
     * @return string
     */
    public function html($string)
    {
        return $this->direct($string);
    }

    /**
     * Escapes url using htmlspecialchars
     *
     * @param string $url URL to escape
     * @return string
     */
    public function url($url)
    {
        return $this->direct($url);
    }
}
