<?php
namespace JonTemplate\Plugin;

/**
 * Plugin to truncate a string. Based on Smarty truncate modifier.
 * Purpose:  Truncate a string to a certain length if necessary,
 *           optionally splitting in the middle of a word, and
 *           appending the $etc string or inserting $etc into the middle.
 * @link http://smarty.php.net/manual/en/language.modifier.truncate.php
 *          truncate (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 */
class Truncate extends PluginAbstract
{
    /**
     * Performs the truncation
     *
     * @param string  $string     string to truncate
     * @param int     $length     length to truncate too
     * @param string  $etc        ellipsis characters
     * @param boolean $breakWords false (default) = break at a word boundary
     * @param boolean $middle     determines whether truncation happens at the end of the string (false) or the middle (true)
     * @return string
     */
    public function direct($string, $length = 80, $etc = '…', $breakWords = false, $middle = false)
    {
        if ($length == 0) {
            return '';
        }

        if (strlen($string) > $length) {
            $length -= min($length, strlen($etc));
            if (!$breakWords && !$middle) {
                $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));
            }
            if (!$middle) {
                return substr($string, 0, $length) . $etc;
            } else {
                return substr($string, 0, $length/2) . $etc . substr($string, -$length/2);
            }
        } else {
            return $string;
        }
    }

    /**
     * Truncate method call (alias)
     *
     * @return string
     */
    public function truncate()
    {
        return call_user_func_array(array($this, 'direct'), func_get_args());
    }
}
