<?php
namespace JonTemplate\Plugin;

/**
 * template plugin to format money
 */
class Money extends PluginAbstract
{
    /**
     * Correctly formats money into float or not depending on whether whole pounds or pounds and pence
     *
     * @param string $string   money string
     * @param string $currency currency string
     * @return string
     * @throws \UnexpectedValueException
     */
    public function direct($string, $currency = '&pound;')
    {
        if (!is_numeric($string)) {
            throw new \UnexpectedValueException('Non-numeric argument given');
        }
        if ((strpos($string, '.') !== false) && preg_match('/^[\d]{1,}\.[\d]{1,}$/', $string)) {
            return $currency . sprintf('%.2f', $string);
        }
        return $currency . $string;
    }

    /**
     * Format call
     *
     * @param string $string   money string
     * @param string $currency currency string
     * @return string
     */
    public function format($string, $currency = '&pound;')
    {
        return $this->direct($string, $currency);
    }
}
