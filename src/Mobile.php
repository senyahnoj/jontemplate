<?php
namespace JonTemplate;

/**
 * Helper for identifying whether the request is coming from a mobile browsing device
 */
class Mobile
{
    /**
     * @var boolean whether or not the client request is from a mobile device
     */
    protected $_isMobile;

    /**
     * @var array of _SERVER request headers
     */
    protected $_server;

    /**
     * Returns whether or not the client is a mobile device (lazy-loading)
     *
     * @return boolean
     */
    public function getIsMobile()
    {
        if (!isset($this->_isMobile)) {
            $this->_isMobile = false;

            $server = $this->getServer();

            if (isset($server['HTTP_X_WAP_PROFILE']) || isset($server['HTTP_PROFILE'])) {
                $this->_isMobile = true;
            } elseif (isset($server['HTTP_USER_AGENT'])
                && preg_match(
                    '/(Alcatel|Asus|Android|BlackBerry|BOLT|Ericsson|Fennec|Fly|Huawei|i-mate|iPAQ|iPhone|iPod|ipad|Iris|Kindle|LG-|LGE-|MDS_|MOT-|Mobile Safari|Maemo|Minimo|Nokia|Opera Mini|Opera Mobi|Palm|Panasonic|Pantech|Philips|Sagem|Samsung|Sharp|SIE-|Symbian|Vodafone|Voxtel|webOS|Windows CE|Windows Phone|ZTE-)/i',
                    $server['HTTP_USER_AGENT']
                )
            ) {
                $this->_isMobile = true;
            } elseif (isset($server['HTTP_ACCEPT'])
                && (
                    (strpos($server['HTTP_ACCEPT'], 'text/vnd.wap.wml') !== false)
                    || (strpos($server['HTTP_ACCEPT'], 'application/vnd.wap.xhtml+xml') !== false)
                )
            ) {
                $this->_isMobile = true;
            }
        }
        return $this->_isMobile;
    }

    /**
     * Sets whether or not the request is from a mobile device
     *
     * @param boolean $isMobile whether or not this is from a mobile device
     * @return Mobile
     * @throws \InvalidArgumentException
     */
    public function setIsMobile($isMobile)
    {
        if (!is_bool($isMobile)) {
            throw new \InvalidArgumentException('Expected boolean argument');
        }
        $this->_isMobile = $isMobile;
    }

    /**
     * Sets the array of server headers
     *
     * @param array $server the array of server headers
     * @return Mobile
     */
    public function setServer(array $server)
    {
        $this->_server = $server;
        return $this;
    }

    /**
     * Returns the array of server headers (lazy-loading)
     *
     * @return array
     */
    public function getServer()
    {
        isset($this->_server) or $this->_server = $_SERVER;
        return $this->_server;
    }
}
