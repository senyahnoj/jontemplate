<?php
namespace JonTemplate;

/**
 * class for an individual template section
 */
class Section
{
    /**
     * @const cache directory to force/unforce javascript and css caching
     */
    const CACHE_B = 20160109;

    /**
     * @const file extension for template files
     */
    const TPL_EXT = '.php';

    /**
     * @var string path to template file
     */
    private $_template;

    /**
     * @var  array of subsections of template
     */
    private $_section = array();

    /**
     * array of assigned template vars
     * @var array
     */
    private $_tplVars = array();

    /**
     * @var boolean whether or not the client is a mobile device
     */
    protected $_isMobile;

    /**
     * @var array of plugin instances
     */
    protected $_plugins = array();

    /**
     * @var array of plugin paths
     */
    protected static $_pluginPaths = array(
        'JonTemplate\\Plugin',
    );

    /**
     * Constructor - sets up the path to the template file
     *
     * @param string $tplPath
     * @param array $tplVars array of template variables - optional at this stage
     */
    public function __construct($tplPath, array $tplVars = array())
    {
        $this->_setTemplate($tplPath);
        $this->_tplVars = $tplVars;
    }

    /**
     * Sets the template path
     *
     * @param string $tplPath full path to the template
     */
    private function _setTemplate($tplPath)
    {
        $renderer = new Template();
        $this->_template = Template::$templateDir . $tplPath . static::TPL_EXT;
    }

    /**
     * Allocates a template section
     *
     * @param string $sectionName
     * @param mixed $tpl template object or a string of inline HTML
     * @return Section
     */
    public function setSection($sectionName, $tpl)
    {
        $this->_section[$sectionName] = $tpl;
        return $this;
    }

    /**
     * Returns a rendered template section
     *
     * @param string $sectionName name of section
     * @return string rendered output
     */
    private function _fetchSection($sectionName)
    {
        $section =& $this->_section[$sectionName];
        if (isset($section)) {
            if ($section instanceof Section) {
                return $section->fetch();
            } else {
                return $section;
            }
        }
    }

    /**
     * Magic method for setting template variables
     *
     * @param string $key   key
     * @param mixed  $value value
     * @return Section
     */
    public function __set($key, $value)
    {
        if (strpos($key, '_') === 0) {
            throw new Exception('Overloaded template properties may not start with an underscore: "' . $key . '"');
        }
        $this->_tplVars[$key] = $value;
        return $this;
    }

    /**
     * Magic method for checking whether inaccessible template properties are set
     *
     * @param string $key key to check
     * @return mixed
     */
    public function __isset($key)
    {
        return isset($this->_tplVars[$key]);
    }

    /**
     * Magic method for retrieving inaccessible template properties
     *
     * @param string $key key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->_tplVars[$key];
    }

    /**
     * Unset inaccessible template property
     *
     * @param string $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->_tplVars[$key]);
    }

    /**
     * Inaccessible methods are mapped to template plugins. If no arguments are given then the plugin instance is returned.
     * Otherwise the direct method is called (if exists).
     *
     * @param string $name      name of method
     * @param array  $arguments arguments
     * @return mixed
     */
    public function __call($methodName, array $arguments = null)
    {
        return call_user_func_array(
            array(
                $this,
                'getPlugin',
            ),
            (!empty($arguments) ? array_merge(array($methodName), $arguments) : array($methodName))
        );
    }

    /**
     * Fetches template
     *
     * @return string
     */
    public function fetch()
    {
        ob_start();
        include($this->_template);
        return ob_get_clean();
    }

    /**
     * Returns a partial. template vars need to be specifically assigned.
     *
     * @param string $tplPath template path
     * @param array  $params  parameters for partial
     * @return string
     */
    private function _partial($tplPath, array $params = array())
    {
        $partial = new Section($tplPath, $params);
        return $partial->fetch();
    }

    /**
     * Returns the named template as an include. inherits all the assigned template vars from the parent template.
     *
     * @param string $tplPath template path
     * @return string
     */
    private function _include($tplPath)
    {
        $include = new Section($tplPath, $this->_tplVars);
        return $include->fetch();
    }

    /**
     * Adds a new plugin path
     *
     * @param string $path plugin path
     * @return void
     */
    public static function addPluginPath($path)
    {
        static::$_pluginPaths[] = $path;
    }

    /**
     * Factory method to instantiate the named plugin and return the instance/direct method call
     *
     * @return mixed
     * @throws \Exception
     */
    private function _plugin()
    {
        return call_user_func_array(
            array(
                $this,
                'getPlugin',
            ),
            func_get_args()
        );
    }

    /**
     * Returns the names plugin/view helper
     *
     * @param string $pluginName name of the plugin
     * @return Plugin\PluginAbstract
     */
    public function getPlugin($pluginName)
    {
        $plugin =& $this->_plugins[$pluginName];
        if (!isset($plugin)) {
            foreach (static::$_pluginPaths as $path) {
                $className = $path . '\\' . ucfirst($pluginName);
                if (class_exists($className)) {
                    $plugin = forward_static_call(array($className, 'getInstance'));
                    break;
                }
            }

            if (!isset($plugin)) {
                throw new \Exception('Plugin ' . $pluginName . ' not found');
            }
        }

        // If called with arguments and there is a direct method call it
        if ((count(func_get_args()) > 1) && method_exists($plugin, 'direct')) {
            return call_user_func_array(
                array(
                    $plugin,
                    'direct',
                ),
                array_slice(func_get_args(), 1)
            );
        }

        return $plugin;
    }

    /**
     * Returns whether or not the client is a mobile device
     *
     * @return boolean
     */
    public function getIsMobile()
    {
        if (!isset($this->_isMobile)) {
            $mobile = new Mobile();
            $mobile->setServer($_SERVER);
            $this->_isMobile = $mobile->getIsMobile();
        }
        return $this->_isMobile;
    }
}
