<?php
namespace JonTemplate;

/**
 * template class
 */
class Template
{
    /**
     * @var string template directory
     */
    public static $templateDir;

    /**
     * @var boolean HTTP caching
     */
    protected $_noCache = false;

    /**
     * @var string Last modified date/time
     */
    protected $_lastModified;

    /**
     * @const TEXT mime type
     */
    const MIME_TEXT = 0;

    /**
     * @const HTML mime type
     */
    const MIME_HTML = 1;

    /**
     * @const XML mime type
     */
    const MIME_XML = 2;

    /**
     * @var int Mime type for HTTP headers
     */
    protected $_mimeType;

    /**
     * @var string CHARSET
     */
    protected $_charset = 'utf-8';

    /**
     * Constructor - sets the mime type of the document
     *
     * @param int $mimeType mime type (from constant)
     */
    public function __construct($mimeType = self::MIME_HTML)
    {
        $this->_mimeType = $mimeType;
    }

    /**
     * Set template directory
     *
     * @param string $templateDir path to template directory
     * @return void
     */
    public static function setTemplateDir($templateDir)
    {
        self::$templateDir = $templateDir;
    }

    /**
     * Display template
     *
     * @param Section $tpl         section for main page layout
     * @param array   $subSections array of sub-sections each one a Section
     * @return void
     */
    public function display(Section $tpl, array $subSections = array())
    {
        $this->_httpHeaders();

        foreach ($subSections as $sectionName => $tplObj) {
            $tpl->setSection($sectionName, $tplObj);
        }
        $template = $tpl->fetch();
        echo $template;
    }

    /**
     * Sets last-modified http header value defaults to now with no arguments
     *
     * @param \DateTime $lastModified last modified time
     * @return Template
     */
    public function setLastModified(\DateTime $lastModified = null)
    {
        $this->_lastModified = $lastModified;
        return $this;
    }

    /**
     * Returns last modified time (lazy-loading)
     *
     * @return \DateTime
     */
    public function getLastModified()
    {
        isset($this->_lastModified) or $this->setLastModified(new \DateTime());
        return $this->_lastModified;
    }

    /**
     * Generic no cache and last-modified headers (in the right order for FF3)
     * see FF3 issue:
     *       https://bugzilla.mozilla.org/show_bug.cgi?id=327790#c8
     *       http://forums.mozillazine.org/viewtopic.php?f=25&t=673135&start=30
     * if content precedes smarty then these don't get activated
     *
     * @return void
     */
    protected function _httpHeaders()
    {
        if (!headers_sent()) {
            if ($this->_noCache) {
                header('Cache-control: no-store, no-cache, must-revalidate');
                header('Pragma: no-cache');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            }
            switch ($this->_mimeType) {
                case self::MIME_HTML:
                    header('Content-type: text/html; charset=' . $this->_charset);
                    break;
                case self::MIME_XML:
                    header('Content-type: text/xml; charset=' . $this->_charset);
                    break;
                case self::MIME_TEXT:
                    header('Content-type: text/plain; charset=' . $this->_charset);
                    break;
            }
            header('Last-modified: ' . $this->getLastModified()->format('D, d M Y H:i:s e'));
        }
    }
}
