<?php
namespace JonTemplate;

/**
 * Unit Test for Mobile platform spotter
 */
class MobileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test that a load of common mobile user agents are recognised
     *
     * The list of user agents was scraped from http://www.useragentstring.com/pages/Mobile%20Browserlist/
     *
     * @return void
     */
    public function testMobileUserAgents()
    {
        $agentsFile = __DIR__ . '/mobile-user-agents.txt';
        $this->_testAgentsFile($agentsFile, true);
    }

    /**
     * Test that a load of common desktop user agents are recognised
     *
     * The list of user agents was scraped from http://www.zytrax.com/tech/web/browser_ids.htm
     *
     * @return void
     */
    public function testDesktopUserAgents()
    {
        $agentsFile = __DIR__ . '/desktop-user-agents.txt';
        $this->_testAgentsFile($agentsFile, false);
    }

    /**
     * Test for other http headers which indicate that it's a mobile device
     *
     * @return void
     */
    public function testHttpHeaders()
    {
        // List of server configurations which indicate mobile devices
        $servers = array(
            array(
                'HTTP_X_WAP_PROFILE' => true,
            ),
            array(
                'HTTP_PROFILE' => true,
            ),
            array(
                'HTTP_ACCEPT' => 'text/vnd.wap.wml,text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            ),
            array(
                'HTTP_ACCEPT' => 'application/vnd.wap.xhtml+xml,text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            ),
        );
        foreach ($servers as $server) {
            $instance = new Mobile();
            $instance->setServer($server);

            $this->assertEquals(
                $instance->getIsMobile(),
                true,
                'Failed assertion for ' . print_r($server, true)
            );
        }

        // Test for desktop HTTP_ACCEPT
        $instance = new Mobile();
        $instance->setServer(
            array(
                'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            )
        );
        $this->assertEquals(
            $instance->getIsMobile(),
            false,
            'Failed assertion for desktop HTTP_ACCEPT string'
        );
    }

    /**
     * Loops through the given user agents file and assert whether or not it should be interpreted as a mobile browser
     *
     * @param string  $agentsFile path to a text file containing user agents
     * @param boolean $isMobile   expected response
     * @return void
     */
    private function _testAgentsFile($agentsFile, $isMobile)
    {
        if (($handle = fopen($agentsFile, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, "\t")) !== false) {
                $userAgent = &$data[0];
                $instance = new Mobile();
                $server = array(
                    'HTTP_USER_AGENT' => $userAgent,
                );
                $instance->setServer($server);
                $this->assertEquals(
                    $instance->getIsMobile(),
                    $isMobile,
                    'Failed assertion for ' . $userAgent
                );
            }
        }
    }
}
