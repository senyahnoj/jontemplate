<?php
namespace JonTemplate\Plugin;

/**
 * Unit Test for the escaping plugin
 */
class EscapeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Escape instance of the escape plugin
     */
    protected $_escape;

    /**
     * Test of the direct method
     *
     * @return void
     */
    public function testDirect()
    {
        $this->_testEscape('direct');
    }

    /**
     * Test of the html method
     *
     * @return void
     */
    public function testHtml()
    {
        $this->_testEscape('html');
    }

    /**
     * Test of the url method
     *
     * @return void
     */
    public function testUrl()
    {
        $this->_testEscape('url');
    }

    /**
     * Test of the given method assuming it's a direct escape
     *
     * @param string $methodName name of method
     * @return void
     */
    protected function _testEscape($methodName)
    {
        $this->assertEquals(
            $this->_escape->$methodName("O'Reilly"),
            'O&#039;Reilly'
        );
    }

    /**
     * Sets up instance of the escape plugin
     *
     * @return void
     */
    protected function setUp()
    {
        $this->_escape = new Escape();
    }
}
