<?php
namespace JonTemplate\Plugin;

/**
 * Unit Test for the truncate plugin
 */
class TruncateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Truncate instance of the truncation class
     */
    protected $_truncate;

    /**
     * Test of the direct method with a short string
     *
     * @return void
     */
    public function testTruncateDefaultShortString()
    {
        $shortString = 'Test of truncation with a short string';
        $this->assertEquals(
            $this->_truncate->truncate($shortString),
            $shortString
        );
    }

    /**
     * Test of the direct method with a short string
     *
     * @return void
     */
    public function testTruncateDefaultLongString()
    {
        $string = 'this many ';
        $this->assertEquals(
            $this->_truncate->truncate(str_repeat($string, 9)),
            trim(str_repeat($string, 8)) . '…'
        );
    }

    /**
     * Tests that passing 0 into the truncation length returns an empty string
     *
     * @return void
     */
    public function testTruncateZeroLength()
    {
        $this->assertEquals(
            $this->_truncate->truncate('Any string', 0),
            ''
        );
    }

    /**
     * Set up method - sets the instance of Truncate
     *
     * @return void
     */
    protected function setUp()
    {
        $this->_truncate = new Truncate();
    }
}
