<?php
namespace JonTemplate\Plugin;

/**
 * Unit Test for the cache buster
 */
class CacheBusterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CacheBuster instance of the cache buster plugin
     */
    protected $_cacheBuster;

    /**
     * Test of the toString method
     *
     * @return void
     */
    public function testToString()
    {
        $this->_cacheBuster->setCacheBuster('62gd7dga87y7g');
        $this->assertEquals(
            (string) $this->_cacheBuster,
            '62gd7dga87y7g'
        );
    }

    /**
     * Test that it doesn't allow you to put spaces in it
     *
     * @expectedException \UnexpectedValueException
     * @return void
     */
    public function testSpaces()
    {
        $this->_cacheBuster->setCacheBuster('this is a test');
    }

    /**
     * Test that it doesn't allow non alphanumeric characters
     *
     * @expectedException \UnexpectedValueException
     * @return void
     */
    public function testNonAlphaNumeric()
    {
        $this->_cacheBuster->setCacheBuster('Nonalpha?!');
    }

    /**
     * Test that it doesn't allow non ASCII characters
     *
     * @expectedException \UnexpectedValueException
     * @return void
     */
    public function testNonAscii()
    {
        $this->_cacheBuster->setCacheBuster('Lémons');
    }

    /**
     * Sets up instance of the cache buster plugin
     *
     * @return void
     */
    protected function setUp()
    {
        $this->_cacheBuster = new CacheBuster();
    }
}
