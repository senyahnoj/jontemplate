<?php
namespace JonTemplate\Plugin;

/**
 * Unit Test for the money formatting plugin
 */
class MoneyTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Money instance of the money plugin
     */
    protected $_money;

    /**
     * Test of the direct method
     *
     * @return void
     */
    public function testDirect()
    {
        $this->_testFormat('direct');
    }

    /**
     * Test of the format method
     *
     * @return void
     */
    public function testFormat()
    {
        $this->_testFormat('format');
    }

    /**
     * Test of the given method assuming it's a direct formatting function
     *
     * @param string $methodName name of method
     * @return void
     */
    protected function _testFormat($methodName)
    {
        $this->assertEquals(
            $this->_money->$methodName(7889.778, '&dollar;'),
            '&dollar;7889.78'
        );
    }

    /**
     * Sets up instance of the money plugin
     *
     * @return void
     */
    protected function setUp()
    {
        $this->_money = new Money();
    }
}
