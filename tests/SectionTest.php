<?php
namespace JonTemplate;

/**
 * Unit Test for the section
 */
class SectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test for getPlugin() calls
     *
     * @return void
     */
    public function testGetPluginCalls()
    {
        $section = new Section('fakeTemplate');

        $this->assertInstanceOf(
            'JonTemplate\\Plugin\\Escape',
            $section->getPlugin('Escape')
        );

        $this->assertEquals(
            $section->getPlugin('Escape', 'foobar'),
            'foobar'
        );
    }

    /**
     * Test for the magic call method which is a front-end for getPlugin()
     *
     * @return void
     */
    public function testCallMethod()
    {
        $section = new Section('fakeTemplate');

        $this->assertInstanceOf(
            'JonTemplate\\Plugin\\Escape',
            $section->escape()
        );

        $this->assertEquals(
            $section->escape('foobar'),
            'foobar'
        );
    }
}
